package com.dwk.qa.utils.jmeter;

import java.io.File;

/**
 * 将jtl文件转换成聚合报告
 * 
 * @author 董文科
 *
 */
public class JTLGenerator {

	public static void main(String[] args) throws Exception {
		System.out.println(
				"Label\tconcurrency\tNO.\t# Samples\tAverage\tMedian\t90% Line\t95% Line\t99% Line\tMin\tMax\tError %\tThroughput\tReceived KB/sec\tSent KB/sec");
		work();

	}

	public static void work() throws Exception {
		File basedir = new File("E:\\results");
		File[] jtls = FileUtil.searchFile(basedir, "jtl");

		for (File file : jtls) {

			String calculator = MyAverageReportGenerator.calculator(file);

			System.err.println(calculator);
		}
	}

}
