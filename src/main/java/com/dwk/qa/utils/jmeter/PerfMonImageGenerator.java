package com.dwk.qa.utils.jmeter;

import java.io.File;
import java.io.IOException;

import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Priority;

import kg.apc.jmeter.vizualizers.PerfMonGui;

/**
 * perfmon插件生成的结果文件转换成图片
 * 
 * @author 董文科
 *
 */
public class PerfMonImageGenerator {

	static {
		LoggingManager.setPriority(Priority.INFO);
		kg.apc.emulators.TestJMeterUtils.createJmeterEnv();
	}

	/**
	 * 将perfmon文件转换成png文件
	 * 
	 * @param src
	 * @param dest
	 * @throws IOException
	 */
	public static void perfMon2png(File src, File dest) throws IOException {
		PerfMonGui panel = new PerfMonGui();
		panel.setFile(src.getAbsolutePath());
		panel.stateChanged(null);
		panel.getGraphPanelChart().saveGraphToPNG(dest, 1300, 550);
	}

	public static void main(String[] args) throws Exception {
		// File src = new
		// File("E:\\results\\1.创建讨论组\\1.创建讨论组-perfMon-50-300-1-false-20170519-135424.csv");
		// File dest = new
		// File("E:\\results\\1.创建讨论组\\1.创建讨论组-perfMon-50-300-1-false-20170519-135424.png");
		// perfMon2png(src, dest);
		work();
	}

	public static void work() throws Exception {
		File basedir = new File("E:\\results");
		String destdir = "E:\\results\\perfmonImage\\";
		File dest = new File(destdir);
		if (!dest.exists()) {
			dest.mkdirs();
		}
		File[] perfmonFiles = FileUtil.searchFile(basedir, "csv");

		for (File file : perfmonFiles) {

			perfMon2png(file, new File(destdir + file.getName().replace(".csv", ".png")));

		}
	}

}
