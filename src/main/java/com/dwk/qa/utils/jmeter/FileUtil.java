package com.dwk.qa.utils.jmeter;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FileUtil {

	/**
	 * 查找指定目录及其子目录中的指定类型的所有文件
	 * 
	 * @param directory
	 * @param fileType
	 * @return
	 */
	public static File[] searchFile(File directory, final String fileType) {
		// 结果
		Set<File> result = new HashSet<File>();

		// 如果不是文件夹 直接返回空数组
		if (!directory.isDirectory()) {
			return new File[0];
		}

		// 列出当前目录下的所有目录和jtl文件
		File[] listFiles = directory.listFiles(new FileFilter() {

			public boolean accept(File file) {

				return file.isDirectory() || file.getName().endsWith(fileType);
			}
		});

		// 遍历结果
		for (File file : listFiles) {

			// 如果是目录继续查找
			if (file.isDirectory()) {
				result.addAll(Arrays.asList(searchFile(file, fileType)));
			} else {// 否则添加该文件
				result.add(file);
			}
		}

		return result.toArray(new File[result.size()]);
	}

}
